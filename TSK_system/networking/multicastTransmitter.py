import socket


def send(message):
    # when Python script and Unity3D are working in the same network but not the same devices
    #MCAST_GRP = '239.39.39.39'
    # when Python script and Unity3D are working at the same PC
    MCAST_GRP = 'localhost'
    MCAST_PORT = 5007
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
    sock.setsockopt(socket.IPPROTO_IP, socket.IP_MULTICAST_TTL, 2)
    sock.sendto(message, (MCAST_GRP, MCAST_PORT))
