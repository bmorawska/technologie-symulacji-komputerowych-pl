from datetime import datetime


def json_data(coordinates_no_filter, coordinates_with_filter):
    now = datetime.now()
    timestamp = datetime.timestamp(now)

    json = {
                "timestamp": timestamp,
                "coordinates_no_filter":
                [
                    {"x": coordinates_no_filter[0]},
                    {"y": coordinates_no_filter[1]}
                ],
                "coordinates_with_filter" :
                [
                    {"x": coordinates_with_filter[0]},
                    {"y": coordinates_with_filter[1]}
                ]
            }
    return json


