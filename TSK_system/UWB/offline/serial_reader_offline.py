import csv
from UWB.filters.my_kalman import KalmanFilterLinear

"""
# Settings
filename = './offline3.csv'  # example test file

tracker_kalman = KalmanFilterLinear()
with open(filename, mode='r') as csv_file:
    readCSV = csv.reader(csv_file, delimiter=',')
    for row in readCSV:
        if row[4] and row[5]:
            center = [float(row[4]), float(row[5])]
            tracker_kalman.step(measurement_vector=center)
            print(tracker_kalman.get_current_state())
            print()

print("Finished!")
"""
# Settings
filename = './decaWaveDaneDoFiltrow.csv'  # example test file
isfirst = True
tracker_kalman2 = KalmanFilterLinear()
with open(filename, mode='r') as csv_file:
    readCSV = csv.reader(csv_file, delimiter=',')
    for row in readCSV:
        if row[0] and row[1]:
            if not isfirst:
                isfirst = False
                vx = tracker_kalman2.get_current_state()[1][0]
                vy = tracker_kalman2.get_current_state()[1][1]
                center = [float(row[0]) + vx, float(row[1]) + vy]
            else:
                center = [float(row[0]), float(row[1])]
            tracker_kalman2.step(measurement_vector=center)
            print(tracker_kalman2.get_current_state()[0])

print("Finished!")