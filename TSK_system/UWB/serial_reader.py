import serial
import sys
import datetime
import json

from UWB.geometry import count_vertices, count_center
from UWB.globals import INTERRUPTED_BY_USER, \
    SERIAL_PORT_LINUX, \
    BAUDRATE, \
    MILLIMETERS_PER_METER
from networking.multicastTransmitter import send
from networking.json_data import json_data
from UWB.filters.my_kalman import KalmanFilterLinear

# initialization of Kalman tracker
tracker_kalman = KalmanFilterLinear()

counter = 0
while True:
    try:
        port = serial.Serial(SERIAL_PORT_LINUX, BAUDRATE) #sudo chmod 666 /dev/ttyACM0 if this line does not work
        raw_data = port.readline()
        data = raw_data.split()
        range0, range1, range2 = 0, 0, 0

        if data[0] == b'mc':
            mask = int(data[1], 16)

            print(datetime.datetime.now().time())
            if mask & 0x01:
                range0 = int(data[2], 16) / MILLIMETERS_PER_METER
                print('Anchor 0: {} m'.format(range0))
            else:
                print('Anchor 0 not connected')

            if mask & 0x02:
                range1 = int(data[3], 16) / MILLIMETERS_PER_METER
                print('Anchor 1: {} m'.format(range1))
            else:
                print('Anchor 1 not connected')

            if mask & 0x04:
                range2 = int(data[4], 16) / MILLIMETERS_PER_METER
                print('Anchor 2: {} m'.format(range2))
            else:
                print('Anchor 2 not connected')

    except (KeyboardInterrupt, SystemExit):
        print('\nProgram interrupted by user')
        sys.exit(INTERRUPTED_BY_USER)

    if (range0 and range1 and range2) != 0:
        triangle = count_vertices(range0, range1, range2)
        center = count_center(triangle)
        tracker_kalman.step(measurement_vector=center)

        # Transmitting message
        msg = json_data(center, tracker_kalman.get_current_state()[0])
        json_message = json.dumps(msg)
        send(str.encode(json_message))

    counter += 1
    print(counter)


