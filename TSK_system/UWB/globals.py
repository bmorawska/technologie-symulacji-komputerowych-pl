from numpy import array

# Anchor localization
ANCH_0 = array([0, 0])
ANCH_1 = array([1.205, 1.684])
ANCH_2 = array([2.659, 0])

# Error codes
INTERRUPTED_BY_USER = 0

# Connection settings
SERIAL_PORT_LINUX = '/dev/ttyACM0'  # if it does not work please type: sudo chmod 666 /dev/ttyACM0 in terminal
BAUDRATE = 9600
TIMEOUT = 0

# Constants
MILLIMETERS_PER_METER = 1000


# Functions
def sqr(x):
    return x * x
