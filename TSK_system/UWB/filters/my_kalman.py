import numpy as np
from UWB.globals import sqr

T = 0.1  # interval between measurements in seconds

Fi = np.array([
    [1, T, 0, 0],
    [0, 1, 0, 0],
    [0, 0, 1, T],
    [0, 0, 0, 1]
])

H = np.array([
    [1, 0, 0, 0],
    [0, 0, 1, 0]
])

x = np.array([
    [0, 0, 0, 0]
]).T

P = np.array([
    [10, 0, 0, 0],
    [0, 10, 0, 0],
    [0, 0, 10, 0],
    [0, 0, 0, 10]
])

Q = np.array([
    [sqr(sqr(T)) / 4, T * sqr(T) / 2,               0,              0],
    [ T * sqr(T) / 3,         sqr(T),               0,              0],
    [              0,              0, sqr(sqr(T)) / 4, T * sqr(T) / 2],
    [              0,              0,  T * sqr(T) / 2,         sqr(T)]
])


# Implements a linear Kalman filter.
class KalmanFilterLinear:
    def __init__(self):
        self.Fi = Fi  # State transition matrix.
        self.H = H    # Observation matrix.
        self.current_state_estimate = x  # Initial state estimate.
        self.current_prob_estimate = P  # Initial covariance estimate.
        self.Q = Q  # Estimated error in process.
        self.R = np.array([
                            [1, 0],
                            [0, 1]
                          ])

    def get_current_state(self):
        return self.current_state_estimate

    def step(self, measurement_vector):
        measurement_vector = np.array([measurement_vector[0], 0, measurement_vector[1], 0])
        # ---------------------------Prediction step---------------------------------------------
        x_predicted = self.Fi @ self.current_state_estimate
        P_predicted = self.Fi @ self.current_prob_estimate @ np.transpose(self.Fi) + self.Q
        # -----------------------------Update step-----------------------------------------------
        zk = self.H @ measurement_vector
        innovation = zk - (self.H @ x_predicted)
        innovation_covariance = self.H @ P_predicted @ np.transpose(self.H) + self.R
        kalman_gain = P_predicted @ np.transpose(self.H) @ np.linalg.inv(innovation_covariance)
        self.current_state_estimate = x_predicted + kalman_gain @ innovation
        size = self.current_prob_estimate.shape[0]
        self.current_prob_estimate = (np.eye(size) - kalman_gain @ self.H) @ P_predicted

