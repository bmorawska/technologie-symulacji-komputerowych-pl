class ARMAFilter(object):

    def __init__(self, alpha=0.8):
        self.state_x = 0.0
        self.state_y = 0.0
        self.alpha = alpha

    def ARMA_step(self, measurement):
        self.state_x = self.alpha * self.state_x + (1.0 - self.alpha) * measurement[0]
        self.state_y = self.alpha * self.state_y + (1.0 - self.alpha) * measurement[1]
        return self.state_x, self.state_y
