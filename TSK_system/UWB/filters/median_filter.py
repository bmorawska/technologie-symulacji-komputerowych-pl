import numpy as np


class MedianFilter(object):

    def __init__(self, window_size=11):

        if window_size % 2 == 0:
            window_size += 1
            print("Size of window has been changed to odd value")

        self.measuresX = np.zeros(window_size)
        self.measuresY = np.zeros(window_size)
        self._place = window_size // 2 + 1

    def median_step(self, measurement):
        self.measuresX = np.append(self.measuresX, measurement[0])
        self.measuresX = self.measuresX[1:]

        self.measuresY = np.append(self.measuresY, measurement[1])
        self.measuresY = self.measuresY[1:]

        median_x = np.median(self.measuresX)
        median_y = np.median(self.measuresY)

        return median_x, median_y
