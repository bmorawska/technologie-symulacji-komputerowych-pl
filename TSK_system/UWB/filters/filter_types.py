from enum import Enum


class Filter(Enum):
    KALMAN = 0
    MEDIAN = 1
    ARMA = 2
    NONE = -1

