﻿using UnityEngine;
using System.Net.Sockets;
using System;
using System.Net;
using SimpleJSON;

public class ClientUDP : MonoBehaviour
{

    private Socket sock;
    private IPEndPoint ipEndPoint;
    //public string ip = "239.39.39.39";
    public string ip = "localhost";
    private IPAddress ipAddress;
    public int port = 5007;

    public GameObject GO_Rudolf;
    public GameObject GO_Eustachy;

    public float anchor_x;
    public float anchor_y;

    public struct Player 
    {
        public float x;
        public float y;
        
        
    }
    
    [HideInInspector]
    public Player Rudolf; //with Kalman filter

    [HideInInspector] 
    public Player Eustachy; //without Kalman filter

    private string message;

    // Use this for initialization
    void Start()
    {
        Eustachy = new Player();
        Rudolf = new Player();

        try
        {
            // https://docs.microsoft.com/en-us/dotnet/framework/network-programming/how-to-create-a-socket //
            sock = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
            //https://docs.microsoft.com/en-us/dotnet/api/system.net.ipaddress.parse?view=netframework-4.8//
            ipAddress = IPAddress.Parse(ip);
			Debug.Log("Adres = " + ipAddress.ToString() + "; port = " + port);
            ipEndPoint = new IPEndPoint(IPAddress.Any, port);

            // Bind endpoint to the socket
            sock.Bind(ipEndPoint);

            // Add socket to the multicast group
            // SocketOptionLevel - https://docs.microsoft.com/en-us/dotnet/api/system.net.sockets.socketoptionlevel?view=netframework-4.8
            // SocketOptionName - https://docs.microsoft.com/en-us/dotnet/api/system.net.sockets.socketoptionname?view=netframework-4.8
            // Multicast Option - https://docs.microsoft.com/en-us/dotnet/api/system.net.sockets.multicastoption?view=netframework-4.8
            sock.SetSocketOption(SocketOptionLevel.IP, SocketOptionName.AddMembership, new MulticastOption(ipAddress, IPAddress.Any));
			sock.Blocking = false;

        }
        catch (Exception e)
        {
            Debug.Log(e.ToString());
        }            
		Debug.Log("Siec sie uruchomila.");
    }

    // Update is called once per frame
    

    void Update()
    {
        try
        {
            byte[] data = new Byte[1024];
            sock.Receive(data);
            message = System.Text.Encoding.ASCII.GetString(data, 0, data.Length);
            Debug.Log(message);

            JSONObject jsonData = (JSONObject)JSON.Parse(message);

            Eustachy.x = jsonData["coordinates_no_filter"].AsArray[0]["x"];
            Eustachy.y = jsonData["coordinates_no_filter"].AsArray[1]["y"];

            Rudolf.x = jsonData["coordinates_with_filter"].AsArray[0]["x"];
            Rudolf.y = jsonData["coordinates_with_filter"].AsArray[1]["y"];

            float modifier_x = 100.0f / anchor_x;
            float modifier_y = 100.0f / anchor_y;
            GO_Eustachy.transform.position = new Vector3(-250.0f, 0.0f, -50.0f) + 
                                             new Vector3(Eustachy.x * 100.0f * modifier_x, 12.0f, Eustachy.y * 100.0f * modifier_y);
            GO_Rudolf.transform.position = new Vector3(150.0f, 0.0f, -50.0f) + 
                                           new Vector3(Rudolf.x * 100.0f * modifier_x, 12.0f, Rudolf.y * 100.0f * modifier_y);
        }
		catch(SocketException e) 
		{
			//Debug.Log("Wyjątek " + e.ToString());
		}
        catch (Exception e)
        {
            Debug.Log(e.ToString());
        }
    }

    private void OnDestroy()
    {
        // https://docs.microsoft.com/en-us/dotnet/framework/network-programming/using-a-synchronous-client-socket //
        sock.Close();
    }
}